﻿using System;

namespace FF.CES
{
    public interface IComponent
    {
        Type ComponentType { get; }
    }
}