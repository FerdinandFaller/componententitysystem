﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FF.CES
{
    public class Entity
    {
        public string Id;

        private readonly List<IComponent> _components;
        private readonly List<ISystem> _systems;


        public Entity(string id = "Entity")
        {
            Id = id;
            _components = new List<IComponent>();
            _systems = new List<ISystem>();
        }
        
        /// <summary>
        /// Components add themselves to the entity
        /// </summary>
        /// <param name="component"></param>
        public void AddComponent(IComponent component)
        {
            if (!_components.Contains(component))
            {
                _components.Add(component);
            }
        }
        
        public void RemoveComponent(IComponent component)
        {
            if (_components.Contains(component))
            {
                _components.Remove(component);
            }
        }

        public IComponent GetComponent(Type type)
        {
            return _components.Where(component => component.ComponentType == type).FirstOrDefault();
        }

        /// <summary>
        /// Systems add themselves to the entity
        /// </summary>
        /// <param name="system"></param>
        public void AddSystem(ISystem system)
        {
            if (!_systems.Contains(system))
            {
                _systems.Add(system);
            }
        }
        
        public void RemoveSystem(ISystem system)
        {
            if (_systems.Contains(system))
            {
                _systems.Remove(system);
            }
        }

        public void Update()
        {
            foreach (var system in _systems)
            {
                system.Update();
            }
        }
    }
}