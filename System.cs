﻿namespace FF.CES
{
    public abstract class System : ISystem
    {
        public Entity Entity { get; private set; }


        protected System(Entity entity)
        {
            entity.AddSystem(this);
            Entity = entity;
        }
        
        public abstract void Update();
    }
}