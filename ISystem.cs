﻿namespace FF.CES
{
    public interface ISystem
    {
        // TODO: Das gefällt mir noch nicht, da es immer von der Entity gesetzt werden muss.
        Entity Entity { get; }

        void Update();
    }
}