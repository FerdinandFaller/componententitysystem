﻿namespace FF.CES.Test
{
    public class MovementSystem : System
    {
        private PositionComponent _positionComponent;
        private VelocityComponent _velocityComponent;


        public MovementSystem(Entity entity) : base(entity)
        {
            _positionComponent = (PositionComponent)Entity.GetComponent(typeof(PositionComponent));
            _velocityComponent = (VelocityComponent)Entity.GetComponent(typeof(VelocityComponent));
        }

        public override void Update()
        {
            if (_positionComponent == null) _positionComponent = (PositionComponent)Entity.GetComponent(typeof (PositionComponent));
            if (_velocityComponent == null) _velocityComponent = (VelocityComponent)Entity.GetComponent(typeof(VelocityComponent));
            if (_positionComponent == null || _velocityComponent == null) return;

            _positionComponent.X += _velocityComponent.X;
            _positionComponent.Y += _velocityComponent.Y;
        }
    }
}