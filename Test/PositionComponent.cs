﻿using System;

namespace FF.CES.Test
{
    public sealed class PositionComponent : Component
    {
        public override Type ComponentType { get; protected set; }

        public float X;
        public float Y;


        public PositionComponent(Entity entity, float x = 0f, float y = 0f) : base(entity)
        {
            ComponentType = GetType();

            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return X + " / " + Y;
        }
    }
}
