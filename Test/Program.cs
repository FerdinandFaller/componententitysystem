﻿using System;

namespace FF.CES.Test
{
    class Program
    {
        static void Main()
        {
            var entity = new Entity("MovingThing");
            var pos = new PositionComponent(entity);
            var vel = new VelocityComponent(entity, 1.2f, 1.5f);
            var move = new MovementSystem(entity);

            Console.WriteLine("Start Pos: " + pos);

            for (int i = 0; i < 10; i++)
            {
                entity.Update();
            }

            Console.WriteLine("End Pos: " + pos);

            Console.ReadKey();
        }
    }
}
