﻿using System;

namespace FF.CES
{
    public abstract class Component : IComponent
    {
        public abstract Type ComponentType { get; protected set; }


        protected Component(Entity entity)
        {
            entity.AddComponent(this);
        }
    }
}